package user

import (
  _ "encoding/json"
)

type Entity struct {
  Name string `json:"name"`
  Email string `json:"email"`
}
