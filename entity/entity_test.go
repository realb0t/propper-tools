package entity

import (
  "testing"
)

func TestCreateEntity(t *testing.T) {
  _ = Entity{"entityId", "path/to/this/entity"}
}

func TestNewEntity(t *testing.T) {
  _ = NewEntity("entityId", "path/to/this/entity")
}