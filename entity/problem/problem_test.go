package problem

import (
  "testing"
  "encoding/json"
)

func TestCreateByConstructor(t *testing.T) {
  _ = NewProblem("entityId", "path/to/this/entity", "", "", 1, 1, 1)
}

func TestMakeProblemByJson(t *testing.T) {
  entityTitle := "Problem title"
  userName := "realb0t"
  problemJson := []byte(`{
      "title": "` + entityTitle + `",
      "customers": [{ "name": "` + userName + `", "email": "kazantsev.nickolay@gmail.com" }],
      "performers": [{ "name": "` + userName + `", "email": "kazantsev.nickolay@gmail.com" }],
      "weight": 1,
      "estimate": 1,
      "level": 0,
      "solving": null,
      "solvings": []
    }`)

  entity := NewProblem("entityId", "path/to/this/entity", "", "", 1, 1, 1)
  err := json.Unmarshal(problemJson, &entity) 

  if err != nil {
    t.Error("Error JSON")
  }

  if entity.GetTitle() != entityTitle {
    t.Error("Title [" + entity.GetTitle() + "] is not equal entityTitle [" + entityTitle + "]")
  }

  if entity.Customers[0].Name != userName {
    t.Error("Customer name is not equal userName")
  }
}