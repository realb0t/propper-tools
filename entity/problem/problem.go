package problem

import (
  base "github.com/realb0t/propper/entity"
  "github.com/realb0t/propper/entity/user"
  "github.com/realb0t/propper/utils/helpers"
  "os"
  "path"
  "encoding/json"
  "io/ioutil"
)

type Entity struct {
  base.Entity
  base.Content
  Title string `json:"title"`
  Customers []*user.Entity `json:"customers"`
  Performers []*user.Entity `json:"performers"`
  Weight int `json:"weight"`
  Estimate int `json:"estimate"`
  Level int `json:"level"`
  Solving string `json:"solving"`
  Solvings []string `json:"solvings"`
}

func CreateEnviropment(entity *Entity) error {
  problemDirPath := entity.Path
  metaFilePath   := path.Join(problemDirPath, ".propperio")
  descFilePath   := path.Join(entity.Path, "..", entity.EntityId + "_problem.md")

  errMkdir := os.MkdirAll(problemDirPath, 0755)
  if errMkdir != nil {
    return errMkdir
  }

  jsonData, errJson := json.MarshalIndent(entity, "", "  ")
  if errJson != nil {
    return errJson
  }

  errMeta := ioutil.WriteFile(metaFilePath, jsonData, 0644)
  if errMeta != nil {
    return errMeta
  }

  errDesc := ioutil.WriteFile(descFilePath, []byte(""), 0644)
  return errDesc
}

func (p *Entity) GetTitle() string {
  return p.Title
}

func NewProblem(id, p, title, content string, weight, estimate, level int) *Entity {
  return &Entity{base.Entity{ 
      EntityId: id, 
      Path: p,
    },
    base.Content{
      helpers.DefaultStrValue(content, "Undefined content"),
    },
    helpers.DefaultStrValue(title, "Undefined title"),   // Title
    make([]*user.Entity, 0), // Customers
    make([]*user.Entity, 0), // Peformers
    helpers.DefaultIntValue(weight, 0),    // Weight
    helpers.DefaultIntValue(estimate, 1),    // Estimate
    helpers.DefaultIntValue(level, 1),    // Level
    "none",   // Solving
    make([]string, 0), // Solvings
  }
}
