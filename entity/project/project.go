package project

import (
  base "github.com/realb0t/propper/entity"
  "github.com/realb0t/propper/utils/helpers"
  "github.com/realb0t/propper/entity"
  "github.com/realb0t/propper/utils"
  "io/ioutil"
  "os"
  "path"
  "encoding/json"
  "github.com/libgit2/git2go"
)

type Entity struct {
  entity.Entity
  Name string `json:"name"`
  Description string `json:"description"`
  DependencyDir string `json:"dependency"`
}

func NewProject(id, entityPath, name, description, dependencyDir string) *Entity {
  return &Entity{
    base.Entity{ 
      EntityId: id, 
      Path: entityPath,
    },
    helpers.DefaultStrValue(name, "Undefined name"),
    helpers.DefaultStrValue(description, "Undefined project description"),
    helpers.DefaultStrValue(dependencyDir, "submodules"),
  }
}

func CreateEnviropment(entity *Entity) error {
  projectDirPath := entity.Path
  metaFilePath   := path.Join(projectDirPath, ".propperio")

  errMkdir := os.MkdirAll(projectDirPath, 0755)
  if errMkdir != nil {
    return errMkdir
  }

  jsonData, errJson := json.MarshalIndent(entity, "", "  ")
  if errJson != nil {
    return errJson
  }

  errFile := ioutil.WriteFile(metaFilePath, jsonData, 0644)
  if errFile != nil {
    return errFile
  }

  repo, errGir := git.InitRepository(projectDirPath, false)
  if errGir != nil {
    return errGir
  }

  return utils.CommitAll(repo, "Create project")
}

func (p *Entity) GetTitle() string {
  return p.Name
}