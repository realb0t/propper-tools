package entity

import (
  "encoding/json"
)

type Interface interface {
  Serialize() ([]byte, error)
  Id() string
  GetTitle() string
}

func NewEntity(id, path string) *Entity {
  return &Entity{id, path}
}

type Entity struct {
  EntityId string `json:"-"`
  Path string `json:"-"`
}

type Content struct {
  Body string `json:"-"`
}

func (self *Entity) Id() string {
  return self.EntityId
}

func (self *Entity) Serialize() ([]byte, error) {
  return json.Marshal(self)
}
