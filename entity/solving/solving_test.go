package solving

import (
  "testing"
  "fmt"
  "encoding/json"
)

func TestCreateByConstructor(t *testing.T) {
  _ = NewSolving("entityId", "path/to/this/entity", "", "")
}

func TestMakeSolvingByJson(t *testing.T) {
  entityTitle := "Solving title"
  userName := "realb0t"
  solvingJson := []byte(`{
      "title": "` + entityTitle + `",
      "author": { "name": "` + userName + `", "email": "kazantsev.nickolay@gmail.com" },
      "critics": [{ "name": "` + userName + `", "email": "kazantsev.nickolay@gmail.com" }],
      "preSolving": null,
      "problems": []
    }`)
  entity := NewSolving("entityId", "path/to/this/entity", "", "")
  err := json.Unmarshal(solvingJson, &entity) 
  
  if err != nil {
    t.Error("Error JSON")
  }

  if entity.GetTitle() != entityTitle {
    fmt.Print(entity)
    t.Error("Title \"" + entity.GetTitle() + "\" is not equal entityTitle \"" + entityTitle + "\"")
  }

  if entity.Critics[0].Name != userName {
    t.Error("Critic name is not equal userName")
  }

  if entity.Author.Name != userName {
    t.Error("Author name is not equal userName")
  }
}

func TestMakeJsonBySolving(t *testing.T) {
  oldEntity := NewSolving("entityId", "path/to/this/entity", "", "")
  solvingJson, merr := json.Marshal(oldEntity)

  if merr != nil {
    t.Error("Error JSON marshalize")
  }

  entity := NewSolving("entityId", "path/to/this/entity", "", "")
  err := json.Unmarshal(solvingJson, &entity) 

  if err != nil {
    t.Error("Error JSON unmarchalize")
  }

  if entity.GetTitle() != oldEntity.GetTitle() {
    fmt.Print(entity)
    t.Error("Title \"" + entity.GetTitle() + "\" is not equal entityTitle \"" + oldEntity.GetTitle() + "\"")
  }

  if len(entity.Critics) > 0 {
    t.Error("Critics is not empty")
  }

  if entity.Author.Name != "" {
    t.Error("Author name is not equal userName")
  }
}