package solving

import (
  base "github.com/realb0t/propper/entity"
  "github.com/realb0t/propper/entity/user"
  "github.com/realb0t/propper/utils/helpers"
  "os"
  "path"
  "encoding/json"
  "io/ioutil"
)

type Entity struct {
  base.Entity
  base.Content
  Title string `json:"title"`
  Author *user.Entity `json:"author"`
  Critics []*user.Entity `json:"critics"`
  PreSolving string `json:"presolving"`
  Problems []string `json:"problems"`
}

func CreateEnviropment(entity *Entity) error {
  solvingDirPath := entity.Path
  metaFilePath   := path.Join(solvingDirPath, ".propperio")
  descFilePath   := path.Join(entity.Path, "..", entity.EntityId + "_solving.md")

  errMkdir := os.MkdirAll(solvingDirPath, 0755)
  if errMkdir != nil {
    return errMkdir
  }

  jsonData, errJson := json.MarshalIndent(entity, "", "  ")
  if errJson != nil {
    return errJson
  }

  errMeta := ioutil.WriteFile(metaFilePath, jsonData, 0644)
  if errMeta != nil {
    return errMeta
  }

  errDesc := ioutil.WriteFile(descFilePath, []byte(""), 0644)
  return errDesc
}

func NewSolving(id, p, title, content string) *Entity {
  return &Entity{base.Entity{ 
      EntityId: id, 
      Path: p,
    },
    base.Content{
      helpers.DefaultStrValue(content, "Undefined content"),
    },
    helpers.DefaultStrValue(title, "Undefined title"),   // Title
    &user.Entity{}, // Author
    make([]*user.Entity, 0), // Critics
    "",    // PreSolving
    make([]string, 0), // Problems
  }
}

func (s *Entity) GetTitle() string {
  return s.Title
}