package definition

import "github.com/realb0t/propper/entity"

type Entity struct {
  entity.Entity
  entity.Content
}

func NewDefinition(id, p string) *Entity {
  return &Entity{entity.Entity{ 
      EntityId: id, 
      Path: p,
    },
    entity.Content{ 
      "",
    },
  }
}