package main

import (
  "os"
  "fmt"
  _ "strings"
  "github.com/codegangsta/cli"
  "github.com/realb0t/propper/operation"
  "github.com/realb0t/propper/entity/problem"
  _ "github.com/deiwin/interact"
)

func main() {
  defer func() {
    if err := recover(); err != nil {
      fmt.Println("Error:", err)
    }
  }()

  currentPath := func() string {
    cPath, err := os.Getwd()
    if err != nil {
      panic(err)
    }
    return cPath
  }

  contextByCli := func(c *cli.Context) *operation.Context {
    context, err := operation.NewCliContext(c)
    if err != nil { panic(err) }
    return context
  }

  pathFlag := cli.StringFlag{
    Name: "path, p",
    Value: currentPath(),
    Usage: "Project path. Default is current path.",
    EnvVar: "PROPPER_PROJECT_PATH",
  }

  app := cli.NewApp()
  app.Commands = []cli.Command{
    {
      Name: "problems",
      Usage: "Get info about Project path",
      Flags: []cli.Flag { pathFlag },
      Action: func(c *cli.Context) {
        cnx := contextByCli(c)
        op := operation.NewOperation(cnx)
        problems, err := op.Scan("problem")

        if err != nil { panic(err) }

        for _, p := range(problems) {
          switch p := p.(type) {
            case *problem.Entity:
              fmt.Printf(" %v - %v \n", p.EntityId, p.Title)
            default:
              panic("Error")
          } 
        }
      },
    }, {
      Name: "info",
      Usage: "Get info about Project path",
      Flags: []cli.Flag { pathFlag },
      Action: func(c *cli.Context) {
        op := operation.NewOperation(contextByCli(c))
        op.PrintInfo()
      },
    }, {
      Name: "project",
      Usage: "Manipulations with Project object",
      Flags: []cli.Flag {
      },
      Subcommands: []cli.Command{
        {
          Name: "create",
          ArgsUsage: "[projectName]",
          Description: "Create new project with projectName attribute",
          Flags: []cli.Flag {
            cli.StringFlag{
              Name: "name",
              Value: "",
              Usage: "Project name",
            }, pathFlag,
            cli.StringFlag{
              Name: "description, desc",
              Value: "",
              Usage: "Project description",
            },
            cli.StringFlag{
              Name: "dependency, dep",
              Value: "",
              Usage: "Dependency dir name",
            },
          },
          Before: func(c *cli.Context) error {
            if c.Args().Present() && c.Args().First() != "" {
              return nil
            } else {
              panic("Not define Project name")
            }
          }, 
          Action: func(c *cli.Context) {
            op := operation.NewOperation(contextByCli(c))
            project, err := op.Create("project", c)
            if err != nil { panic(err) }
            fmt.Printf("Project created: %v\n", project.Id())
          },
        }, {
          Name: "edit",
          Action:  func(c *cli.Context) {
            fmt.Println("edit action", c)
          },
        }, {
          Name: "destroy",
          Action:  func(c *cli.Context) {
            fmt.Println("destroy action", c)
          },
        }, {
          Name: "show",
          Action:  func(c *cli.Context) {
            fmt.Println("show action", c)
          },
        }, {
          Name: "doctor",
          Usage: "Check project structure and validate meta files",
          Action:  func(c *cli.Context) {
            fmt.Println("doctor action", c)
          },
        }, {
          Name: "push",
          Usage: "Push changes into remote repository",
          Action:  func(c *cli.Context) {
            fmt.Println("doctor action", c)
          },
        }, {
          Name: "pull",
          Usage: "Sync project wih remote repository",
          Action:  func(c *cli.Context) {
            fmt.Println("doctor action", c)
          },
        },
      },
    }, {
      Name: "problem",
      Usage: "Manipulations with Problem objects",
      Subcommands: []cli.Command{
        {
          Name: "create",
          Flags: []cli.Flag {
            cli.StringFlag{
              Name: "title",
              Value: "",
              Usage: "Problem title",
            }, pathFlag,
            cli.StringFlag{
              Name: "content",
              Value: "",
              Usage: "Problem content",
            },
            cli.IntFlag{
              Name: "weight",
              Value: 0,
              Usage: "Problem weight",
            },
            cli.IntFlag{
              Name: "estimate",
              Value: 0,
              Usage: "Problem estimate",
            },
            cli.IntFlag{
              Name: "level",
              Value: 0,
              Usage: "Problem level",
            },
          },
          Before: func(c *cli.Context) error {
            if c.Args().Present() && c.Args().First() != "" {
              return nil
            } else {
              panic("Not define Problem name")
            }
          }, 
          Action: func(c *cli.Context) {
            op := operation.NewOperation(contextByCli(c))
            problem, err := op.Create("problem", c)
            if err != nil { panic(err) }
            fmt.Printf("Problem created: %v\n", problem.Id())
          },
        }, {
          Name: "edit",
          Action:  func(c *cli.Context) {
            fmt.Println("edit action", c)
          },
        }, {
          Name: "destroy",
          Action:  func(c *cli.Context) {
            fmt.Println("destroy action", c)
          },
        }, {
          Name: "show",
          Action:  func(c *cli.Context) {
            fmt.Println("show action", c)
          },
        },
      },
    }, {
      Name: "solving",
      Usage: "Manipulations with Solving objects",
      Subcommands: []cli.Command{
        {
          Name: "create",
          Flags: []cli.Flag {
            cli.StringFlag{
              Name: "title",
              Value: "",
              Usage: "Solving title",
            }, pathFlag,
            cli.StringFlag{
              Name: "content",
              Value: "",
              Usage: "Solving content",
            },
          },
          Before: func(c *cli.Context) error {
            if c.Args().Present() && c.Args().First() != "" {
              return nil
            } else {
              panic("Not define Problem name")
            }
          }, 
          Action: func(c *cli.Context) {
            op := operation.NewOperation(contextByCli(c))
            problem, err := op.Create("solving", c)
            if err != nil { panic(err) }
            fmt.Printf("Solving created: %v\n", problem.Id())
          },
        }, {
          Name: "edit",
          Action:  func(c *cli.Context) {
            fmt.Println("edit action", c)
          },
        }, {
          Name: "destroy",
          Action:  func(c *cli.Context) {
            fmt.Println("destroy action", c)
          },
        }, {
          Name: "show",
          Action:  func(c *cli.Context) {
            fmt.Println("show action", c)
          },
        },
      },
    }, {
      Name: "definition",
      Usage: "Manipulations with Defenition objects",
      Subcommands: []cli.Command{
        {
          Name: "create",
          Action:  func(c *cli.Context) {
            fmt.Println("create action", c)
          },
        }, {
          Name: "edit",
          Action:  func(c *cli.Context) {
            fmt.Println("edit action", c)
          },
        }, {
          Name: "destroy",
          Action:  func(c *cli.Context) {
            fmt.Println("destroy action", c)
          },
        }, {
          Name: "show",
          Action:  func(c *cli.Context) {
            fmt.Println("show action", c)
          },
        },
      },
    }, {
      Name: "dependency",
      Usage: "Manipulations with Dependency objects",
      Subcommands: []cli.Command{
        {
          Name: "create",
          Action:  func(c *cli.Context) {
            fmt.Println("create action", c)
          },
        }, {
          Name: "edit",
          Action:  func(c *cli.Context) {
            fmt.Println("edit action", c)
          },
        }, {
          Name: "destroy",
          Action:  func(c *cli.Context) {
            fmt.Println("destroy action", c)
          },
        }, {
          Name: "show",
          Action:  func(c *cli.Context) {
            fmt.Println("show action", c)
          },
        },
      },
    },
  }

  app.Run(os.Args)
}