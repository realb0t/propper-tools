package operation

import (
  "testing"
  "github.com/realb0t/propper/entity/user"
  "github.com/libgit2/git2go"
)

func TestNewOperation(t *testing.T) {
  u := &user.Entity{ "Kazantsev Nickola", "realbot@mail.ru" }
  _ = NewOperation(NewContext(u, ".", ".", "name", &git.Repository{}))
}
