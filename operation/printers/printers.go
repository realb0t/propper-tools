package Printers

import (
  "github.com/realb0t/propper/entity"
)

type Printer interface {
  Print() string
}

/// Printer "PrintId"

type PrintId struct {
  entity entity.Interface
}

func NewPrintId(e entity.Interface) *PrintId {
  return &PrintId{ e }
}

func (p *PrintId) Print() string {
  return p.entity.Id()
}

// Output

type Output struct {
  Printers []Printer
}

func NewOutput(e entity.Interface) *Output {
  _ = map[string]Printer{ 
    "entityId": NewPrintId(e),
  }

  out := &Output{make([]Printer, 0)}
  return out
}

func (o Output) Print(e entity.Interface) string {
  outputStr := ""
  for _, p := range(o.Printers) {
    outputStr = outputStr + p.Print()
  }

  return outputStr
}