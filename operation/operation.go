package operation

import (
  "github.com/realb0t/propper/entity/user"
  "github.com/realb0t/propper/entity"
  "github.com/realb0t/propper/entity/solving"
  "github.com/realb0t/propper/entity/problem"
  "github.com/realb0t/propper/entity/project"
  "github.com/realb0t/propper/utils"
  "io/ioutil"
  "strings"
  "path"
  "path/filepath"
  "fmt"
  "os"
  "regexp"
  "encoding/json"
  "github.com/codegangsta/cli"
  "github.com/libgit2/git2go"
)

type Context struct {
  user *user.Entity
  path string
  entityPath string
  entityId string
  repository *git.Repository
}

type Operation struct {
  context *Context
  entity entity.Interface
  attachPaths []string
  description []byte
  metaJson []byte
}

type OperationError struct {
  Reason string
  Operation *Operation
}

func (e OperationError) Error() string {
  return fmt.Sprintf("OperationError - %v", e.Reason)
}

/**
 * Check that operation executing in PropperIO project directory
 */
func (op *Operation) CheckPropperStruct() bool {
  return utils.IsFileExist(op.context.entityPath)
}

func NewContext(u *user.Entity, contextPath, entityPath, entityName string, repo *git.Repository) *Context {
  return &Context{ u, contextPath, entityPath, entityName, repo }
}

func NewCliContext(cliContext *cli.Context) (*Context, error) {
  var entityName string
  args := cliContext.Args()
  if len(args) > 0 {
    entityName = args[0]
  }
  pathParam  := cliContext.String("path")
  repo, err  := utils.OpenRepository(pathParam)

  var (
    userEntity *user.Entity
    errUser error
  )

  if err == nil {
    userEntity, errUser = utils.LocalGitUser(repo)
  } else {
    userEntity, errUser = utils.GlobalGitUser()
  }

  if errUser != nil { 
    return nil, errUser
  }

  contextPath := pathParam
  entityPath := path.Join(pathParam, entityName)

  return NewContext(userEntity, contextPath, entityPath, entityName, repo), nil
}

func NewOperation(c *Context) *Operation {
  operation := &Operation{c, nil, []string{}, []byte{}, []byte{}}
  return operation
}

func (op *Operation) PrintInfo() {
  if op.context.repository != nil {
    fmt.Println("Exist repository path: ", op.context.repository.Path())
    fmt.Println("Entity path: ", op.context.entityPath)
  } else {
    fmt.Println("Without repository")
    fmt.Println("Entity path: ", op.context.entityPath)
  }
  
  fmt.Println("User Name: ", op.context.user.Name)
  fmt.Println("User Email: ", op.context.user.Email)
}

func (op *Operation) Scan(entityType string) ([]entity.Interface, error) {
  entities := make([]entity.Interface, 0)
  projectRoot, err := utils.ProjectRoot(op.context.path)

  if err != nil {
    return entities, err
  }

  entityPaths := []string{}

  err = filepath.Walk(projectRoot, func(p string, f os.FileInfo, err error) error {
    _, filename := filepath.Split(p)
    isProblemFile, _ := filepath.Match("*_" + entityType + ".md", filename)
    if isProblemFile {
      entityPaths = append(entityPaths, normalizeEntityPath(p))
    }

    return nil
  })

  if err != nil {
    return entities, err
  }

  fmt.Println("Problems:")

  for _, entityPath := range(entityPaths) {
    loadedEntity, err := op.loadEntity(entityType, entityPath)
    if err != nil {
      return entities, err 
    }
    entities = append(entities, loadedEntity)
  }

  return entities, err
}

func (op *Operation) Create(entityType string, c *cli.Context) (entity.Interface, error) {
  switch entityType {
    case "project": 
      return op.createProject(c)
    case "problem":
      return op.createProblem(c)
    case "solving":
      return  op.createSolving(c)
    default:
      return nil, OperationError{"Can't create Entity with type " + entityType, op}
  }
}

func (op *Operation) createProject(c *cli.Context) (entity.Interface, error) {
  if op.CheckPropperStruct() {
    return nil, OperationError{"Project exist in path " + op.context.entityPath, op}
  }
  fmt.Printf("Create project with name %v\n", c.String("name"))
  projectEntity := project.NewProject(op.context.entityId, op.context.entityPath, 
    c.String("name"), c.String("description"), c.String("dependency"))
  err := project.CreateEnviropment(projectEntity)
  return projectEntity, err
}

func (op *Operation) createProblem(c *cli.Context) (entity.Interface, error) {
  if !utils.IsIntoProjectDir(op.context.path) {
    return nil, OperationError{"Path is not Propper project " + op.context.entityPath, op}
  }

  problemEntity := problem.NewProblem(op.context.entityId, op.context.entityPath,
    c.String("title"), c.String("content"), c.Int("weight"),
    c.Int("estimate"), c.Int("level"))

  problemEntity.Customers = []*user.Entity{ op.context.user }

  err := problem.CreateEnviropment(problemEntity)
  return problemEntity, err
}

func (op *Operation) createSolving(c *cli.Context) (entity.Interface, error) {
  if !utils.IsIntoProjectDir(op.context.path) {
    return nil, OperationError{"Path is not Propper project " + op.context.entityPath, op}
  }

  solvingEntity := solving.NewSolving(op.context.entityId, op.context.entityPath,
    c.String("title"), c.String("content"))

  solvingEntity.Author = op.context.user

  err := solving.CreateEnviropment(solvingEntity)
  return solvingEntity, err
}

func normalizeEntityPath(descFilePath string) string {
  re := regexp.MustCompile("(_(problem|solving)\\.md)")
  return re.ReplaceAllString(descFilePath, "")
}

func (op *Operation) loadEntity(entityType, entityPath string) (entity.Interface, error) {
  
  var (
    content []byte
    existEntity entity.Interface
  )

  metaPath := path.Join(entityPath, ".propperio")
  metaJson, err := ioutil.ReadFile(metaPath)
  _, entityId := filepath.Split(entityPath)

  switch entityType {
    case "problem":
      problemEntity := problem.NewProblem(entityId, entityPath, "", "", 0, 0, 0)
      err = json.Unmarshal(metaJson, problemEntity)
      if err != nil { return nil, err }
      descFilePath := entityPath + "_problem.md"
      content, err = ioutil.ReadFile(descFilePath)
      problemEntity.Body = string(content)
      existEntity = problemEntity 
    case "solving":
      solvingEntity := solving.NewSolving(entityId, entityPath, "", "")
      err = json.Unmarshal(metaJson, &solvingEntity)
      if err != nil { return nil, err }
      descFilePath := entityPath + "_solving.md"
      content, err = ioutil.ReadFile(descFilePath)
      solvingEntity.Body = string(content)
      existEntity = solvingEntity
    default:
      return nil, OperationError{"Can't load Entity with type " + entityType, op}
  }

  return existEntity, err
}

/**
 * path - Object logic path (not file path)
 */
func (op *Operation) load(entityPath string) (entity.Interface, error) {
  var err error
  var metaJson []byte

  entityCode     := path.Base(entityPath)
  entityBaseDir  := path.Dir(entityPath)
  problemPath    := path.Join(entityBaseDir, entityCode + "_problem.md")
  solvingPath    := path.Join(entityBaseDir, entityCode + "_solving.md")
  contents, err  := ioutil.ReadDir(entityPath)

  if err != nil { return nil, err }
  if utils.IsFileExist(problemPath) && utils.IsFileExist(solvingPath) {
    return nil, OperationError{"Problem and Solving names equals", op}
  }

  for _, element := range(contents) {
    elPath := path.Join(entityPath, element.Name())
    switch {
      default:
        op.attachPaths = append(op.attachPaths, elPath)
      case element.IsDir():
      case strings.Contains(element.Name(), "meta"):
      case strings.Contains(element.Name(), "_definition"):
      case strings.Contains(element.Name(), "_problem"):
      case strings.Contains(element.Name(), "_solving"):
        continue
    }
  }

  op.metaJson, err = ioutil.ReadFile(path.Join(entityPath, "meta.json"))
  if err != nil { return nil, OperationError{"Not find meta.json for entity " + entityCode, op} }

  if utils.IsFileExist(problemPath) {
    var problem *problem.Entity
    err := json.Unmarshal(metaJson, &problem)
    if err != nil { return nil, OperationError{"Invalid Meta file for Problem " + entityCode, op} }
    op.entity = problem
    op.description, err = ioutil.ReadFile(problemPath)
    if err != nil { return nil, OperationError{"Cant read Description file for Problem " + entityCode, op} }
  } else {
    var solving *solving.Entity
    err := json.Unmarshal(metaJson, &solving)
    if err != nil { return nil, OperationError{"Invalid Meta file for Solving " + entityCode, op} }
    op.entity = solving
    op.description, err = ioutil.ReadFile(solvingPath)
    if err != nil { return nil, OperationError{"Cant read Description file for Solving " + entityCode, op} }
  }

  return op.entity, err
}
