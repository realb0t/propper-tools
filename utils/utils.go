package utils

import (
  "github.com/realb0t/propper/entity/user"
  "os"
  "path"
  "errors"
  _ "encoding/json"
  _ "github.com/codegangsta/cli"
  "github.com/libgit2/git2go"
)

// Open exist repository by path
func OpenRepository(repPath string) (*git.Repository, error) {
  repo, err := git.OpenRepository(repPath)
  if err != nil {
    baseProjectPath, err := git.Discover(repPath, true, make([]string, 0))
    
    if err != nil {
      return nil, err
    }

    repo, err = git.OpenRepository(baseProjectPath)
  }

  return repo, err
}

// Create user by exist local repository
func LocalGitUser(repo *git.Repository) (*user.Entity, error) {
  sig, err := repo.DefaultSignature()
  if err != nil { return nil, err }
  return &user.Entity{ sig.Name, sig.Email }, nil
}

// Create user by global git config
func GlobalGitUser() (*user.Entity, error) {
  confPath, err := git.ConfigFindGlobal()
  if err != nil { return nil, err }
  config, err := git.OpenOndisk(&git.Config{}, confPath)
  if err != nil { return nil, err }
  itt, _ := config.NewIterator()
  var name, email string

  for field, _ := itt.Next(); field != nil; field, _ = itt.Next() {
    if field.Name == "user.name" {
      name = field.Value
    }
    if field.Name == "user.email" {
      email = field.Value
    } 
  }

  return &user.Entity{name, email}, nil
}

func IsFileExist(filePath string) bool {
  _, err := os.Stat(filePath)

  if err != nil {
    return os.IsExist(err)
  } else {
    return true
  }
}

func CommitAll(repo *git.Repository, msg string) error {
  sig, err := repo.DefaultSignature()
  idx, err := repo.Index()
  if err != nil {
    return err
  }

  err = idx.AddAll([]string{ "." }, git.IndexAddDefault, nil)
  if err != nil {
    return err
  }

  treeId, err := idx.WriteTree()
  if err != nil {
    return err
  }

  err = idx.Write()
  if err != nil {
    return err
  }

  tree, err := repo.LookupTree(treeId)
  if err != nil {
    return err
  }

  var commitTarget *git.Commit
  ref, _ := repo.Head()
  if ref != nil {
    targetOid := ref.Target()
    if targetOid != nil {
      commitTarget, err = repo.LookupCommit(targetOid)
      if err != nil {
        return err
      }
    }
  }

  message := "Create project"
  if commitTarget != nil {
    _, err = repo.CreateCommit("HEAD", sig, sig, message, tree, commitTarget)
  } else {
    _, err = repo.CreateCommit("HEAD", sig, sig, message, tree)
  }

  return err
}

func ProjectRoot(currentPath string) (string, error) {
  baseProjectPath, err := git.Discover(currentPath, true, make([]string, 0))

  return currentPath, err

  metaFilePath := path.Join(baseProjectPath, "..", ".propperio")

  if IsFileExist(metaFilePath) {
    return metaFilePath, nil
  } else {
    return currentPath, errors.New("Is not Propper project")
  }
}


func IsIntoProjectDir(currentPath string) bool {
  _, err := ProjectRoot(currentPath)

  if err != nil {
    return false
  } else {
    return true
  }
}