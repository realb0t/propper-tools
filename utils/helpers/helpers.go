package helpers

func DefaultStrValue(currentVal, defaultVal string) string {
  if currentVal != "" {
    return currentVal
  } else {
    return defaultVal
  } 
}

func DefaultIntValue(currentVal, defaultVal int) int {
  if currentVal != 0 {
    return currentVal
  } else {
    return defaultVal
  } 
}